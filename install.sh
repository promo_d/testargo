#!/bin/bash
set -x

cd initk8s
kubectl create ns $2
bash init.sh $2
cd ..
sleep 25
### install helm
helm install -n $1 a9kts-helm --namespace $2
