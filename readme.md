
#### init container to test connections
kubectl run -it --rm --restart=Never mongotest --image=mongo:3.4.14 bash
kubectl run -it --rm --restart=Never platformtest --image=dockerhub.internal-iot83.com/analyticsplatform:latest bash
kubectl run -it --rm --restart=Never jobrctest --image=dockerhub.internal-iot83.com/analyticsjobrpc:latest bash
kubectl run -it --rm --restart=Never jobstatstest --image=dockerhub.internal-iot83.com/analyticsjobstats:latest bash
kubectl run -it --rm --restart=Never cassandratest --image=cassandra:3.11.2 bash

#### helm install commands
helm install -n a9kts a9kts   ## release name a9kts is hardcoded
helm upgrade a9kts a9kts  

